# Perodizer

Dates for recurring tasks made EASY!

Usage
```PHP
$periodizer = new \Periodizer\Periodizer('2019-01-01', '2019-01-31', 'MONTHLY');
    $periodizer
        ->addOpportunity('MONDAY')
        ->toCollection()
        
        /*
        
        */
```

### Types
* WEEKLY (Steps weeks)
* MONTHLY (Steps months)
* YEARLY (Steps years)


### Weekly opportunities
* MONDAY
* TUESDAY
* WEDNESDAY
* THURSDAY
* FRIDAY
* SATURDAY
* SUNDAY

> You could also add the prefix `ODD` or `EVEN` for the obvious effect

### Monthly opportunities
* WEEK 1-5 MONDAY - SUNDAY. `WEEK 2 MONDAY` etc


TODO:
* every x Days
* UNIT TESTS
