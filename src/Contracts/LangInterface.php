<?php
/**
 * Created by PhpStorm.
 * User: install
 * Date: 2019-04-01
 * Time: 23:00
 */

namespace Periodizer\Contracts;


interface LangInterface
{
    public function lang(): array;
}
