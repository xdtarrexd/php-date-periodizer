<?php

namespace Periodizer\Lang;

use Periodizer\Contracts\LangInterface;

class En implements LangInterface
{
    public function lang(): array
    {
        return [
            // Weekdays
            'MONDAY' => 'Monday',
            'TUESDAY' => 'Tuesday',
            'WEDNESDAY' => 'Wednesday',
            'THURSDAY' => 'Thursday',
            'FRIDAY' => 'Friday',
            'SATURDAY' => 'Saturday',
            'SUNDAY' => 'Sunday',
            // Months
            'JANUARY' => 'January',
            'FEBRUARY' => 'February',
            'MARCH' => 'March',
            'APRIL' => 'April',
            'MAY' => 'May',
            'JUNE' => 'June',
            'JULY' => 'July',
            'AUGUST' => 'August',
            'SEPTEMBER' => 'September',
            'OCTOBER' => 'Oktober',
            'NOVEMBER' => 'November',
            'DECEMBER' => 'December',
            // Misc
            'WEEK' => 'week',
            'FIRST' => 'first',
            'LAST' => 'last',
            'DAY' => 'day',
            'ODD' => 'odd',
            'EVEN' => 'even',
            'EVERY' => 'every',
            'NTH' => 'th'
        ];
    }
}
