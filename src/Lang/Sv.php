<?php

namespace Periodizer\Lang;

use Periodizer\Contracts\LangInterface;

class Sv implements LangInterface
{
    public function lang(): array
    {
        return [
            // Weekdays
            'MONDAY' => 'Måndag',
            'TUESDAY' => 'Tisdag',
            'WEDNESDAY' => 'Onsdag',
            'THURSDAY' => 'Torsdag',
            'FRIDAY' => 'Fredag',
            'SATURDAY' => 'Lördag',
            'SUNDAY' => 'Söndag',
            // Months
            'JANUARY' => 'Januari',
            'FEBRUARY' => 'Februari',
            'MARCH' => 'Mars',
            'APRIL' => 'April',
            'MAY' => 'Maj',
            'JUNE' => 'Juni',
            'JULY' => 'July',
            'AUGUST' => 'Augusti',
            'SEPTEMBER' => 'September',
            'OCTOBER' => 'Oktober',
            'NOVEMBER' => 'November',
            'DECEMBER' => 'December',
            // Misc
            'WEEK' => 'vecka',
            'FIRST' => 'första',
            'LAST' => 'sista',
            'DAY' => 'dag',
            'ODD' => 'udda',
            'EVEN' => 'jämn',
            'EVERY' => 'var',
            'NTH' => 'de'
        ];
    }
}
