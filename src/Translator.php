<?php
/**
 * Created by PhpStorm.
 * User: Tarre
 * Date: 2019-04-14
 * Time: 11:13
 */

namespace Periodizer;


class Translator
{
    public static $dictionary;

    /**
     * Translator constructor.
     * @param Contracts\LangInterface $lang
     */
    public function __construct(Contracts\LangInterface $lang)
    {
        self::$dictionary = $lang->lang();
    }

    /**
     * @param $text
     * @return string
     */
    public static function trans($text)
    {
        return strtr($text, self::$dictionary);
    }

}
