<?php
/**
 * Created by PhpStorm.
 * User: install
 * Date: 2019-03-26
 * Time: 21:01
 */

namespace Periodizer;


use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * @property Carbon|string startDate
 * @property Carbon|string stopDate
 * @property string type
 * @property array settings
 */
class Periodizer // implements \ArrayAccess, \JsonSerializable
{
    const PERIOD_TYPE_WEEKLY = 'WEEKLY';
    const PERIOD_TYPE_MONTHLY = 'MONTHLY';
    const PERIOD_TYPE_YEARLY = 'YEARLY';
    const PERIOD_TYPE_PERIOD = 'PERIOD';

    protected $startDate;
    protected $stopDate;
    protected $type;
    protected $settings = [
        'date_as_string' => true,
        'date_format' => 'Y-m-d',
        'crash_inverted_dates' => true
    ];
    protected $rows = []; //in
    protected $map = []; //out


    /**
     * Periodizer constructor.
     * @param $startDate
     * @param $stopDate
     * @param string $type
     * @param array $settings
     */
    public function __construct($startDate, $stopDate, string $type, array $settings = [])
    {

        $this->settings = array_merge($this->settings, $settings);

        if (!strtotime($startDate)) {
            throw new \InvalidArgumentException('invalid time given for $startDate: ' . $startDate);
        }
        if (!strtotime($stopDate)) {
            throw new \InvalidArgumentException('invalid time given for $stopDate: ' . $stopDate);
        }
        if (!$startDate instanceof Carbon) {
            $startDate = Carbon::parse($startDate);
        }
        if (!$stopDate instanceof Carbon) {
            $stopDate = Carbon::parse($stopDate);
        }

        if (!in_array($type, [
            self::PERIOD_TYPE_WEEKLY,
            self::PERIOD_TYPE_MONTHLY,
            self::PERIOD_TYPE_YEARLY,
            self::PERIOD_TYPE_PERIOD,
        ])) {
            throw new \InvalidArgumentException(sprintf('Invalid $type: ' . $type));
        }

        if ($startDate->gt($stopDate) && $this->settings['crash_inverted_dates']) {
            throw new \InvalidArgumentException('$startDate has to be greater than $stopDate');
        }

        $this->startDate = $startDate->startOfDay();
        $this->stopDate = $stopDate->endOfDay();
        $this->type = $type;

    }

    /**
     * @param array $opportunities
     * @return $this
     */
    public function setOpportunities(array $opportunities)
    {
        foreach ($opportunities as $opportunity) {
            $this->addOpportunity($opportunity['opportunity'], $opportunities['payload']);
        }
        return $this;
    }

    /**
     * @param string $opportunity
     * @param null $payload
     * @return $this
     */
    public function addOpportunity(string $opportunity, $payload = null)
    {
        $this->rows[] = [
            'opportunity' => mb_strtoupper($opportunity),
            'payload' => $payload
        ];
        return $this;
    }

    protected function generateWeeks()
    {
        $now = (clone $this->startDate)->startOfWeek();
        $stop = (clone $this->stopDate)->endOfWeek();
        $diffInWeeks = $now->diffInWeeks($stop);

        for ($i = 0; $i < $diffInWeeks + 1; $i++) { // TODO: för att denna skall vara som WHILE -metoden och få sista veckan så måste man lägga en +1 på diffInWeeks. Men, why?? kolla det
            collect($this->rows)
                ->each(function ($row) use (&$now) {
                    $opportunity = $row['opportunity'];
                    $date = null;
                    switch ($opportunity) {
                        case 'MONDAY';
                            $date = $now;
                            break;
                        case 'TUESDAY':
                            $date = $now->addDays(1);
                            break;
                        case 'WEDNESDAY':
                            $date = $now->addDays(2);
                            break;
                        case 'THURSDAY':
                            $date = $now->addDays(3);
                            break;
                        case 'FRIDAY':
                            $date = $now->addDays(4);
                            break;
                        case 'SATURDAY':
                            $date = $now->addDays(5);
                            break;
                        case 'SUNDAY':
                            $date = $now->addDays(6);
                            break;
                    }
                    if ($now->weekOfYear % 2 == 1) {
                        switch ($opportunity) {
                            case 'ODD MONDAY';
                                $date = $now;
                                break;
                            case 'ODD TUESDAY':
                                $date = $now->addDays(1);
                                break;
                            case 'ODD WEDNESDAY':
                                $date = $now->addDays(2);
                                break;
                            case 'ODD THURSDAY':
                                $date = $now->addDays(3);
                                break;
                            case 'ODD FRIDAY':
                                $date = $now->addDays(4);
                                break;
                            case 'ODD SATURDAY':
                                $date = $now->addDays(5);
                                break;
                            case 'ODD SUNDAY':
                                $date = $now->addDays(6);
                                break;
                        }
                    } else {
                        switch ($opportunity) {
                            case 'EVEN MONDAY';
                                $date = $now;
                                break;
                            case 'EVEN TUESDAY':
                                $date = $now->addDays(1);
                                break;
                            case 'EVEN WEDNESDAY':
                                $date = $now->addDays(2);
                                break;
                            case 'EVEN THURSDAY':
                                $date = $now->addDays(3);
                                break;
                            case 'EVEN FRIDAY':
                                $date = $now->addDays(4);
                                break;
                            case 'EVEN SATURDAY':
                                $date = $now->addDays(5);
                                break;
                            case 'EVEN SUNDAY':
                                $date = $now->addDays(6);
                                break;
                        }
                    }

                    // did we find any date? we do not want to add
                    if (!is_null($date)) {
                        $this->map[] = [
                            'date' => clone $date,
                            'payload' => $row['payload']
                        ];
                    }
                    // restart the week for the next opportunity
                    $now = $now->startOfWeek();
                });
            // step to the next week
            $now = $now->addWeek(1)->startOfWeek();
        }
    }

    protected function generateMonths()
    {
        $now = (clone $this->startDate)->startOfMonth();
        $stop = (clone $this->stopDate)->endOfMonth();

        $diffInMonths = (int)ceil($now->daysInMonth / $stop->daysInMonth);

        // Here we can step each month
        for ($i = 0; $i < $diffInMonths; $i++) {
            collect($this->rows)
                ->each(function ($row, $key) use (&$now, &$stop) {
                    $opportunity = $row['opportunity'];
                    $date = null;
                    if (preg_match('/week (\d+) ([a-z]{3})/i', $opportunity, $m)) {
                        $date = $now->copy()->addWeek(($m[1] - 1)); //->startOfWeek()
                        while (mb_strtoupper($date->format('D')) !== $m[2]) {
                            $date = $date->addDay();
                        }
                    } else {
                        switch ($opportunity) {
                            case 'FIRST MONDAY':
                                while (!$now->isMonday()) {
                                    $now = $now->addDay();
                                }
                                $date = $now;
                                break;
                            case 'FIRST TUESDAY':
                                while (!$now->isTuesday()) {
                                    $now = $now->addDay();
                                }
                                $date = $now;
                                break;
                            case 'FIRST WEDNESDAY':
                                while (!$now->isWednesday()) {
                                    $now = $now->addDay();
                                }
                                $date = $now;
                                break;
                            case 'FIRST THURSDAY':
                                while (!$now->isThursday()) {
                                    $now = $now->addDay();
                                }
                                $date = $now;
                                break;
                            case 'FIRST FRIDAY':
                                while (!$now->isFriday()) {
                                    $now = $now->addDay();
                                }
                                $date = $now;
                                break;
                            case 'FIRST SATURDAY':
                                while (!$now->isSaturday()) {
                                    $now = $now->addDay();
                                }
                                $date = $now;
                                break;
                            case 'FIRST SUNDAY':
                                while (!$now->isSunday()) {
                                    $now = $now->addDay();
                                }
                                $date = $now;
                                break;
                            // TODO This will currently ignore the LAST monday if its out of scope. This can be solved by taking the first monday in relative to where the user set the date initially
                            case 'LAST MONDAY':
                                $now = $now->endOfMonth(); // we do not care for the stop date
                                while (!$now->isMonday()) {
                                    $now = $now->subDay();
                                }
                                $date = $now;
                                break;
                            case 'LAST TUESDAY':
                                $now = $now->endOfMonth(); // we do not care for the stop date
                                while (!$now->isTuesday()) {
                                    $now = $now->subDay();
                                }
                                $date = $now;
                                break;
                            case 'LAST WEDNESDAY':
                                $now = $now->endOfMonth(); // we do not care for the stop date
                                while (!$now->isWednesday()) {
                                    $now = $now->subDay();
                                }
                                $date = $now;
                                break;
                            case 'LAST THURSDAY':
                                $now = $now->endOfMonth(); // we do not care for the stop date
                                while (!$now->isThursday()) {
                                    $now = $now->subDay();
                                }
                                $date = $now;
                                break;
                            case 'LAST FRIDAY':
                                $now = $now->endOfMonth(); // we do not care for the stop date
                                while (!$now->isFriday()) {
                                    $now = $now->subDay();
                                }
                                $date = $now;
                                break;
                            case 'LAST SATURDAY':
                                $now = $now->endOfMonth(); // we do not care for the stop date
                                while (!$now->isSaturday()) {
                                    $now = $now->subDay();
                                }
                                $date = $now;
                                break;
                            case 'LAST SUNDAY':
                                $now = $now->endOfMonth(); // we do not care for the stop date
                                while (!$now->isSunday()) {
                                    $now = $now->subDay();
                                }
                                $date = $now;
                                break;
                            case 'FIRST':
                                $date = $now->startOfMonth();
                                break;
                            case 'LAST':
                                $date = $now->endOfMonth();
                                break;
                            case (preg_match('/day\h*(\d+)/i', $opportunity, $m) && $m):
                                $date = $now->startOfMonth()->day($m[1]);
                                break;
                        }
                    }
                    if (!is_null($date)) {
                        $this->map[] = [
                            'date' => clone $date,
                            'payload' => $row['payload']
                        ];
                    }
                    // restart the month for the next opportunity
                    $now = $now->startOfMonth();
                });
            // step to the next month
            $now = $now->addMonth(1)->startOfMonth();
        }
    }

    protected function generateYears()
    {
        $now = (clone $this->startDate)->startOfYear();
        $stop = (clone $this->stopDate)->endOfYear();

        $diffInYears = (int)ceil($now->diffInDays($stop) / 365);

        for ($i = 0; $i < $diffInYears; $i++) {
            collect($this->rows)
                ->each(function ($row) use (&$now) {
                    $opportunity = $row['opportunity'];
                    $date = null;
                    if (preg_match('/(january|february|march|april|may|june|july|august|september|october|november|december)\h*(\d+)/i', $opportunity, $m)) {
                        $month = (function () use ($m) {
                            switch (strtoupper($m[1])) {
                                case 'JANUARY':
                                    return 1;
                                case 'FEBRUARY':
                                    return 2;
                                case 'MARCH':
                                    return 3;
                                case 'APRIL':
                                    return 4;
                                case 'MAY':
                                    return 5;
                                case 'JUNE':
                                    return 6;
                                case 'JULY':
                                    return 7;
                                case 'AUGUST':
                                    return 8;
                                case 'SEPTEMBER':
                                    return 9;
                                case 'OCTOBER':
                                    return 10;
                                case 'NOVEMBER':
                                    return 11;
                                case 'DECEMBER':
                                    return 12;
                            }
                        })();
                        $day = $m[2];
                        $date = $now->month($month)->day($day);
                    }
                    if (!is_null($date)) {
                        $this->map[] = [
                            'date' => clone $date,
                            'payload' => $row['payload']
                        ];
                    }
                    // restart the year for the next opportunity
                    $now = $now->startOfYear();
                });
            $now = $now->addYear(1)->startOfYear();
        }

    }

    protected function generatePeriods()
    {
        collect($this->rows)
            ->each(function ($row) {
                $now = (clone $this->startDate);
                $stop = (clone $this->stopDate);
                while ($now->lte($stop)) {
                    $this->map[] = [
                        'date' => clone $now,
                        'payload' => $row['payload']
                    ];
                    $now->addDay($row['opportunity']);
                }
            });
    }

    protected function postProcess(): Collection
    {
        return collect($this->map)
            ->filter(function ($row) {
                // slice out of bounds
                return $row['date']->between($this->startDate, $this->stopDate, true);
            })->map(function ($row) {
                if ($this->settings['date_as_string']) {
                    $row['date'] = $row['date']->format($this->settings['date_format']);
                }
                return $row;
            })->values();
    }

    /**
     * Get the instance as an array.
     *
     * @return Collection
     */
    public function toCollection()
    {
        switch ($this->type) {
            case self::PERIOD_TYPE_WEEKLY:
                $this->generateWeeks();
                break;
            case self::PERIOD_TYPE_MONTHLY:
                $this->generateMonths();
                break;
            case self::PERIOD_TYPE_YEARLY:
                $this->generateYears();
                break;
            case self::PERIOD_TYPE_PERIOD:
                $this->generatePeriods();
                break;
        }
        return $this->postProcess();
    }
}
