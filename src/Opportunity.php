<?php
/**
 * Created by PhpStorm.
 * User: install
 * Date: 2019-04-01
 * Time: 22:28
 */

namespace Periodizer;


use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Periodizer\Contracts\LangInterface;
use Periodizer\Lang\En;

class Opportunity
{
    protected $weekly;
    protected $monthly;
    protected $yearly;

    protected $translator;
    protected $dateNotes;


    /**
     * Opportunity constructor.
     * @param LangInterface $lang
     * @param array $dateNotes
     */
    public function __construct(LangInterface $lang = null, array $dateNotes = [])
    {
        if (!$lang) {
            $lang = new En;
        }
        $this->translator = new Translator($lang);
        $this->dateNotes = $dateNotes;
    }

    /**
     * @param $text
     * @return string
     */
    protected function trans($text)
    {
        return $this->translator->trans($text);
    }

    /**
     * @return Collection
     */
    public function getWeekly(): Collection
    {
        $weeks = [];
        foreach (['', 'ODD', 'EVEN'] as $prefix) {
            foreach (['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'] as $weekName) {
                $value = $prefix ? $prefix . ' ' . $weekName : $weekName;
                $weeks[] = [
                    'text' => $this->trans($value),
                    'value' => $value
                ];
            }
        }
        return collect($weeks);
    }

    /**
     * @return Collection
     */
    public function getMonthly($maxWeeks = 4): Collection
    {
        $months = [];
        $prefixes = collect(range(0, $maxWeeks - 1))
            ->map(function ($num) {
                return 'WEEK ' . ($num + 1);
            })
            ->merge([
                'FIRST',
                'LAST'
            ])->toArray();

        foreach ($prefixes as $prefix) {
            foreach (['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'] as $weekName) {
                $value = $prefix ? $prefix . ' ' . $weekName : $weekName;
                $months[] = [
                    'text' => $this->trans($value),
                    'value' => $value
                ];
            }
        }

        foreach (range(1, 31) as $day) {
            $value = sprintf('DAY %d', $day);
            $months[] = [
                'text' => $this->trans($value),
                'value' => $value
            ];
        }

        return collect($months);
    }

    /**
     * @return Collection
     */
    public function getYearly(): Collection
    {
        $months = [];
        for ($i = 1; $i < 13; $i++) {
            $dt = Carbon::now()->month($i);
            for ($j = 1; $j < ($dt->daysInMonth + 1); $j++) {
                $cd = $dt->copy()->day($j);
                $cdName = sprintf('%s %d', strtoupper($cd->format('F')), $cd->day);

                $months[] = [
                    'text' => $this->trans($cdName . $this->getNote($cd->format('Y-m-d'))),
                    'value' => $cdName
                ];
            }
        }

        return collect($months);
    }

    /**
     * @param int $min
     * @param int $max
     * @return Collection
     * @throws \Exception
     */
    public function getPeriod($min = 1, $max = 14): Collection
    {
        if ($min < 1) {
            throw new \Exception('$min must be at least 1');
        }
        $ret = [];
        for ($i = $min; $i < $max + 1; $i++) {
            if ($i == 1) {
                $text = $this->trans('EVERY DAY');
            } else {
                $text = $this->trans('EVERY ' . $i . 'NTH DAY');
            }
            $ret[] = [
                'text' => $text,
                'value' => $i,
            ];
        }
        return collect($ret);
    }

    protected function getNote($ymd)
    {
        return isset($this->dateNotes[$ymd]) ? sprintf(' (%s)', $this->dateNotes[$ymd]) : '';
    }

}
