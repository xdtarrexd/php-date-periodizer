<?php
/**
 * Created by PhpStorm.
 * User: install
 * Date: 2019-04-14
 * Time: 20:37
 */

namespace Periodizer;


class PeriodizerMonthlyTest extends \PHPUnit\Framework\TestCase
{

    public function testSingleFullTwoMonths()
    {
        $periodizer = new Periodizer('2019-01-01', '2019-02-28', 'MONTHLY');

        $dates = $periodizer
            ->addOpportunity('FIRST MONDAY')
            ->toCollection()->pluck('date')->toArray();

        $this->assertCount(2, $dates);

        $this->assertTrue($dates[0] == '2019-01-07');
        $this->assertTrue($dates[1] == '2019-02-04');

    }

    public function testMultiWeekFullTwoMonths()
    {
        $periodizer = new Periodizer('2019-01-01', '2019-02-28', 'MONTHLY');

        $dates = $periodizer
            ->addOpportunity('WEEK 1 TUESDAY')
            ->addOpportunity('WEEK 2 TUESDAY')
            ->addOpportunity('WEEK 3 WEDNESDAY')
            ->toCollection()->pluck('date')->toArray();

        $this->assertCount(6, $dates);

        $this->assertTrue($dates[0] == '2019-01-01');
        $this->assertTrue($dates[1] == '2019-01-08');
        $this->assertTrue($dates[2] == '2019-01-16');
        $this->assertTrue($dates[3] == '2019-02-05');
        $this->assertTrue($dates[4] == '2019-02-12');
        $this->assertTrue($dates[5] == '2019-02-20');
    }

    public function testMultiWeekFullTwoMonthsFirstBloodPartTwo()
    {
        $periodizer = new Periodizer('2019-01-01', '2019-02-28', 'MONTHLY');

        $dates = $periodizer
            ->addOpportunity('WEEK 1 THURSDAY')
            ->addOpportunity('WEEK 2 THURSDAY')
            ->addOpportunity('FIRST MONDAY')
            ->addOpportunity('WEEK 3 THURSDAY')
            ->addOpportunity('DAY 2')
            ->addOpportunity('WEEK 4 THURSDAY')
            ->addOpportunity('LAST MONDAY')
            ->toCollection()->pluck('date')->toArray();

        $this->assertCount(14, $dates);

        $shouldYield = [
            '2019-01-03',
            '2019-01-02',
            '2019-02-02',
            '2019-01-07',
            '2019-01-10',
            '2019-01-17',
            '2019-01-24',
            '2019-02-07',
            '2019-02-14',
            '2019-02-21',
            '2019-02-28',
            '2019-02-04',
            '2019-02-25'
        ];

        foreach ($shouldYield as $date) {
            $this->assertTrue(in_array($date, $dates));
        }


    }


}