<?php
/**
 * Created by PhpStorm.
 * User: Tarre
 * Date: 2019-04-14
 * Time: 18:23
 */

namespace Periodizer;


class PeriodizerPerioidTest extends \PHPUnit\Framework\TestCase
{
    public function testTestTesttest()
    {
        $periodizer = new Periodizer('2019-01-01', '2019-01-31', 'PERIOD');

        $dates = $periodizer
            ->addOpportunity('2')
            ->addOpportunity('4')
            ->toCollection()->pluck('date')->toArray();

        $shouldYield = [
            '2019-01-01',
            '2019-01-03',
            '2019-01-05',
            '2019-01-07',
            '2019-01-09',
            '2019-01-11',
            '2019-01-13',
            '2019-01-15',
            '2019-01-17',
            '2019-01-19',
            '2019-01-21',
            '2019-01-23',
            '2019-01-25',
            '2019-01-27',
            '2019-01-29',
            '2019-01-31',
            '2019-01-01',
            '2019-01-05',
            '2019-01-09',
            '2019-01-13',
            '2019-01-17',
            '2019-01-21',
            '2019-01-25',
            '2019-01-29',
        ];

        $shouldNotYield = [
            '2019-04-20'
        ];

        foreach ($shouldYield as $date) {
            $this->assertTrue(in_array($date, $dates));
        }

        foreach ($shouldNotYield as $date) {
            $this->assertFalse(in_array($date, $dates));
        }
    }
}
