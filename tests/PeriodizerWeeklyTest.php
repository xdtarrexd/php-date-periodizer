<?php
/**
 * Created by PhpStorm.
 * User: Tarre
 * Date: 2019-04-14
 * Time: 18:23
 */

namespace Periodizer;


class PeriodizerWeeklyTest extends \PHPUnit\Framework\TestCase
{

    public function testSingleFullMonth()
    {
        $periodizer = new Periodizer('2019-01-01', '2019-01-31', 'WEEKLY');

        $dates = $periodizer->addOpportunity('MONDAY')->toCollection()->pluck('date')->toArray();

        $shouldYield = [
            '2019-01-07',
            '2019-01-14',
            '2019-01-21',
            '2019-01-28',
        ];

        $shouldNotYield = [
            '2018-12-31',
            '2019-02-04'
        ];

        foreach ($shouldYield as $date) {
            $this->assertTrue(in_array($date, $dates));
        }

        foreach ($shouldNotYield as $date) {
            $this->assertFalse(in_array($date, $dates));
        }
    }

    public function testSingleCrossMonth()
    {
        $periodizer = new Periodizer('2019-01-01', '2019-02-14', 'WEEKLY');

        $dates = $periodizer->addOpportunity('TUESDAY')->toCollection()->pluck('date')->toArray();

        $shouldYield = [
            '2019-01-01',
            '2019-01-08',
            '2019-01-08',
            '2019-01-15',
            '2019-01-22',
            '2019-01-29',
            '2019-02-05',
            '2019-02-12',
        ];

        $shouldNotYield = [
            '2019-02-19'
        ];

        foreach ($shouldYield as $date) {
            $this->assertTrue(in_array($date, $dates));
        }

        foreach ($shouldNotYield as $date) {
            $this->assertFalse(in_array($date, $dates));
        }
    }

    public function testMultipleSingleMonth()
    {
        $periodizer = new Periodizer('2019-01-01', '2019-01-31', 'WEEKLY');

        $dates = $periodizer
            ->addOpportunity('WEDNESDAY')
            ->addOpportunity('FRIDAY')
            ->toCollection()->pluck('date')->toArray();

        $shouldYield = [
            '2019-01-02',
            '2019-01-04',
            '2019-01-09',
            '2019-01-11',
            '2019-01-16',
            '2019-01-18',
            '2019-01-23',
            '2019-01-25',
            '2019-01-30'
        ];

        $shouldNotYield = [
            '2019-02-01'
        ];

        foreach ($shouldYield as $date) {
            $this->assertTrue(in_array($date, $dates));
        }

        foreach ($shouldNotYield as $date) {
            $this->assertFalse(in_array($date, $dates));
        }
    }

    public function testEvenSingleMonth()
    {
        $periodizer = new Periodizer('2019-01-01', '2019-01-31', 'WEEKLY');

        $dates = $periodizer
            ->addOpportunity('EVEN THURSDAY')
            ->toCollection()->pluck('date')->toArray();

        $shouldYield = [
            '2019-01-10',
            '2019-01-24'
        ];

        $shouldNotYield = [
            '2019-01-03',
            '2019-01-17',
            '2019-01-31'
        ];

        foreach ($shouldYield as $date) {
            $this->assertTrue(in_array($date, $dates));
        }

        foreach ($shouldNotYield as $date) {
            $this->assertFalse(in_array($date, $dates));
        }
    }

    public function testOddSingleMonth()
    {
        $periodizer = new Periodizer('2019-01-01', '2019-01-31', 'WEEKLY');

        $dates = $periodizer
            ->addOpportunity('ODD FRIDAY')
            ->toCollection()->pluck('date')->toArray();

        $shouldYield = [
            '2019-01-04',
            '2019-01-18'
        ];

        $shouldNotYield = [
            '2019-01-11',
            '2019-01-25'
        ];

        foreach ($shouldYield as $date) {
            $this->assertTrue(in_array($date, $dates));
        }

        foreach ($shouldNotYield as $date) {
            $this->assertFalse(in_array($date, $dates));
        }
    }

    public function testAllCrossMonth()
    {
        $periodizer = new Periodizer('2019-01-01', '2019-02-17', 'WEEKLY');

        $dates = $periodizer
            ->addOpportunity('ODD SATURDAY')
            ->addOpportunity('EVEN SUNDAY')
            ->addOpportunity('WEDNESDAY')
            ->toCollection()->pluck('date')->toArray();

        $shouldYield = [
            '2019-01-05',
            '2019-01-19',
            '2019-02-02',
            '2019-02-16',
            '2019-01-13',
            '2019-01-27',
            '2019-02-10',
            '2019-01-02',
            '2019-01-09',
            '2019-01-16',
            '2019-01-23',
            '2019-01-30',
            '2019-02-06',
            '2019-02-13'
        ];

        $shouldNotYield = [
            '2019-01-01',
            '2019-01-08',
            '2019-01-15',
            '2019-01-22',
            '2019-01-29',
            '2019-02-05',
            '2019-02-12',
            '2019-01-03',
            '2019-01-10',
            '2019-01-17',
            '2019-01-24',
            '2019-01-31',
            '2019-02-07',
            '2019-02-14',
            '2019-01-11',
            '2019-01-25',
            '2019-02-08',
            '2019-02-22',
            '2019-01-06',
            '2019-01-20',
            '2019-02-03'
        ];

        foreach ($shouldYield as $date) {
            $this->assertTrue(in_array($date, $dates));
        }

        foreach ($shouldNotYield as $date) {
            $this->assertFalse(in_array($date, $dates));
        }
    }

}
