<?php
/**
 * Created by PhpStorm.
 * User: Tarre
 * Date: 2019-04-14
 * Time: 18:23
 */

namespace Periodizer;


class PeriodizerYearlyTest extends \PHPUnit\Framework\TestCase
{

    public function testWhateverItsYearsItWontGoWrongZzZ()
    {
        $periodizer = new Periodizer('2019-01-01', '2019-12-31', 'YEARLY');

        $dates = $periodizer
            ->addOpportunity('JANUARY 25')
            ->addOpportunity('MAY 10')
            ->addOpportunity('DECEMBER 31')
            ->toCollection()->pluck('date')->toArray();

        $shouldYield = [
            '2019-01-25',
            '2019-05-10',
            '2019-12-31'
        ];

        $shouldNotYield = [
            '2019-04-20'
        ];

        foreach ($shouldYield as $date) {
            $this->assertTrue(in_array($date, $dates));
        }

        foreach ($shouldNotYield as $date) {
            $this->assertFalse(in_array($date, $dates));
        }
    }

}
